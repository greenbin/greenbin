angular.module("greenBin", ["ngRoute", "ui.bootstrap", "ngAutocomplete", 'uiGmapgoogle-maps', 'ngTagsInput'])
.constant("dataUrl", "http://localhost:2403/products")
.constant("dataUrlUsuario", "http://localhost:2403/usuario")

.controller("greenBinCtrl", function ($scope, $http, dataUrl, dataUrlUsuario, $location){

	$scope.data = {};

		
	$http.get(dataUrl)
		.success(function (data){
			$scope.data.products = data;
		})
		.error(function(error){
			$scope.data.error = error;
		});	

    $scope.postAd = function (adDetails) {
      var ad = angular.copy(adDetails)

      $http.post(dataUrl, ad)
       /* .success(function (data) {
          $scope.data.adId = data.id;
        })*/
        .error(function (error) {
              $scope.data.adError = error;
              })
        .finally(function () {
              $location.path("/home");
              });
        }

    $scope.postUsuario = function (usuarioDetails) {
        var usuario = angular.copy(usuarioDetails);
        
        $http.post(dataUrlUsuario, usuario)
            /*.success(function (data) {
              $scope.data.usuarioId = data.id;
              
              })
            .error(function (error) {
              $scope.data.usuarioError = error;
            })*/.finally(function () {
              $location.path("/home");
              });
        }
})



.controller('CarouselDemoCtrl', function ($scope) {
  $scope.myInterval = 10000;
  var slides = $scope.slides = [];
  $scope.addSlide = function() {
    var newFolder = slides.length + 1;
    slides.push({
      /*image: 'http://placekitten.com/' + newWidth + '/300',
      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]*/
        image: '/images/' + newFolder + '/image-large.png',
      

    });
  };
  for (var i=0; i<2; i++) {
    $scope.addSlide();
  }
})

.controller('TimepickerCtrl', function ($scope, $log) {
  $scope.mytime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 15;

  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };

})

.config(function(tagsInputConfigProvider) { 
  tagsInputConfigProvider
    .setDefaults('tagsInput', {
      	
      minLength: 2,
      maxLength: 15,
      maxTags: 5,
      addOnEnter: true,
      enableEditingLastTag: true,
      addFromAutocompleteOnly: false
    })
    .setDefaults('autoComplete', {
      debounceDelay: 200,
      minLength: 3,
      maxResultsToShow: 5,
      loadOnDownArrow: true,
      loadOnEmpty: true,
      highlightMatchedText: true,
      selectFirstMatch: false
    })

})

.controller('TagsMainCtrl', function($scope, $http) {
  /*$scope.tags = [
    { text: 'Tag1' },
    { text: 'Tag2' },
    { text: 'Tag3' }
  ];*/
   
  $scope.loadTags = function(query) {
    return $http.get('directives/ng-tag/tags.json');
  };
})

.controller('MapTabCtrl',function($scope){
  
  $scope.activeTab1=true;
  $scope.activeTab2=!$scope.activeTab1;
  $scope.activeTab3=false;
  
  $scope.tabTab2 = function()
  {
    console.log("Tab2");
    
    if(document.getElementById("newMap") != null)
    {
    var latlng = new google.maps.LatLng(-27.4710107,153.02344889999995);
    var mapOptions = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(document.getElementById("newMap"), mapOptions);
    }
  }
  
})

.controller('navLinkActiveCtrl', function ($scope, $location) { 
    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
})

//for post ad condition buttons 
.controller('ButtonsCtrl', function ($scope) {
  $scope.singleModel = 1;

  $scope.radioModel = '3';

  $scope.checkModel = {
    '1': false,
    '2': true,
    '3': false,
    '4': false
  };
})

.controller('DatepickerDemoCtrl', function ($scope) {
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  
  $scope.format = 'shortDate';
})

.controller("AutocompleteLocationCtrl",function ($scope) {

   $scope.result = '';
   $scope.options = {
      country: 'au',
      
    };    
    $scope.details = '';

      
  })

.config(function ($routeProvider) {
        $routeProvider.when("/aboutUs", {
          templateUrl: "/views/aboutUs.html"
        });
       
        $routeProvider.when("/home", {
          templateUrl: "/views/home.html"
        });

        $routeProvider.when("/product", {
          templateUrl: "/views/product.html"
        });

        $routeProvider.when("/postAnAd", {
          templateUrl: "/views/postAnAd.html"
        });

            
        $routeProvider.otherwise({
          templateUrl: "/views/home.html"
        });
});
